# sortingApp

In order to run the application you must follow these instructions:

1. Remove the `.sample` from `.env` and replace the values inside with your own configuration.
2. To get all the dependencies, run:
```sh
npm install 
```
3. Create an user in the database so that you can access the routes that need authtentication. in order to do it you must do a `Post` request to the `http://localhost:3000/users` with the `username` and `password` information.
After creating the user you will be automatically logged in the app.

**If you token expires, you can log in by doing a Post request with the credentials `username` and `password` to the following endpoint: `http://localhost:3000/login`**


If you want to run the test suite, you would need to:

1. Create an `.env.test` file and set all the attributes needed for authtentication and database conenction.
2. Run:
```sh
npm test 
```
