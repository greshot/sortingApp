require("dotenv").config();
const express = require("express");
const app = express();
const { setRoutes } = require("./routes");
const cookieParser = require("cookie-parser");
const { connectToDatabase } = require("./utils/database");
const rootDir = __dirname;

const port = process.env.PORT || 3000;

app.use(express.json());
app.use(cookieParser());

setRoutes(app, rootDir);

(async () => {
  try {
    await connectToDatabase();
    app.listen(port, () => {
      console.log(`Server started on port ${port}`);
    });
  } catch (error) {
    console.log("Error establishing database connection: ", error);
  }
})();

module.exports = app;