const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const token = req.cookies.jwtToken;
  if (!token) {
    next();
  } else {
    jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
      if (err) {
        next();
      } else {
        res.sendStatus(200);
      }
    });
  }
};
