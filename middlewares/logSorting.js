const logger = require("../utils/logger");

module.exports = (req, res, next) => {
  const ip =
    (req.headers["x-forwarded-for"] || "").split(",")[0] ||
    req.connection.remoteAddress;

  const sortType = req.url.replace(/\//, "");
  logger.info(`Ip: ${ip}, sort: ${sortType}`);
  next();
};
