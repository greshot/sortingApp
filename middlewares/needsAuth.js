const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const token = req.cookies.jwtToken;
  if (!token) {
    res.status(401).send("Unauthorized!");
  } else {
    jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
      if (err) {
        res.status(401).send("Unauthorized!");
      } else {
        next();
      }
    });
  }
};
