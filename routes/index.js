const express = require("express");
const needsAuth = require("../middlewares/needsAuth");
const checkLogin = require("../middlewares/checkLogin");
const logSorting = require("../middlewares/logSorting");
const path = require("path");

const setRoutes = (app, rootDir) => {
  app.use("/assets", needsAuth, express.static(path.join(rootDir, "assets")));
  app.use("/users", require("./users"));
  app.use("/login", checkLogin, require("./login"));
  app.use("/", needsAuth, logSorting, require("./sort"));
};

module.exports = {
  setRoutes
};
