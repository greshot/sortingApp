const express = require("express");
const router = express.Router();
const { generateToken } = require("../utils/auth");
const User = require("../models/User");

router.post("/", async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(401).json({
        error: "Incorrect username or password"
      });
    }
    const authenticated = await user.isCorrectPassword(password);
    if (!authenticated) {
      return res.status(401).json({
        error: "Incorrect username or password"
      });
    }
    const token = await generateToken(user._id);
    res
      .cookie("jwtToken", token, { httpOnly: true, maxAge: 3600000 })
      .sendStatus(200);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      error
    });
  }
});

module.exports = router;
