const express = require("express");
const router = express.Router();
const { parseOriginalFile, saveSortedData } = require("../utils/files");

router.get("/asc", async (req, res) => {
  try {
    const originalData = await parseOriginalFile();
    const data = originalData.reduce((newList, currentArray) => {
      newList.push(currentArray.sort((a, b) => a - b));
      return newList;
    }, []);
    await saveSortedData(data);
    res.status(200).json({ data });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      error
    });
  }
});

router.get("/des", async (req, res) => {
  try {
    const originalData = await parseOriginalFile();
    const data = originalData.reduce((newList, currentArray) => {
      newList.push(currentArray.sort((a, b) => b - a));
      return newList;
    }, []);
    await saveSortedData(data);
    res.status(200).json({ data });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      error
    });
  }
});

router.get("/mix", async (req, res) => {
  try {
    const mix = index => (index % 2 === 0 ? (a, b) => b - a : (a, b) => a - b);
    const originalData = await parseOriginalFile();
    const data = originalData.reduce((newList, currentArray, currentIndex) => {
      newList.push(currentArray.sort(mix(currentIndex + 1)));
      return newList;
    }, []);
    await saveSortedData(data);
    res.status(200).json({ data });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      error
    });
  }
});

module.exports = router;
