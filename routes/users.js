const express = require("express");
const router = express.Router();
const { generateToken } = require("../utils/auth");
const User = require("../models/User");

router.post("/", async (req, res) => {
  const ip =
    (req.headers["x-forwarded-for"] || "").split(",")[0] ||
    req.connection.remoteAddress;

  try {
    const { name, email, username, password } = req.body;
    const user = new User({
      name,
      email,
      username,
      password
    });
    await user.save();
    const token = await generateToken(user._id);
    res
      .cookie("jwtToken", token, { httpOnly: true, maxAge: 3600000 })
      .sendStatus(201);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      error
    });
  }
});

module.exports = router;
