require("dotenv").config({ path: "./.env.test" });
const User = require("../../models/User");
const chai = require('chai');
const { expect } = chai

describe('User model', () => {

  describe('Create new user', () => {

    it('When username is empty, it should have an username error', () => {
      const user = new User({ password: "53121314" });
      user.validate(error => {
        expect(error.errors).to.have.property("username");
      });
    });

    it('When password´s length is less than 6 characters, it should have an password error', () => {
      const user = new User({ username: "John Doe", password: "1234" });
      user.validate(error => {
        expect(error.errors).to.have.property("password");
      });
    });

    it('When values are valid, it should have no errors', () => {
      const user = new User({ username: "John Doe", password: "123456" });
      user.validate(error => {
        expect(error).to.equals(null);
      });
    });

  });

});