require("dotenv").config({ path: "./.env.test" });
const User = require("../../models/User");
const chai = require('chai');
const { expect } = chai
const { cleanUpDatabase } = require("../../utils/database");
const app = require("../../app");
const http = require('chai-http');

chai.use(http);



describe('Assets route /assets', () => {
  const username = "John Doe";
  const password = "123456";

  before(async () => {
    const user = new User({ username, password });
    await user.save();
  });

  after(async () => {
    await cleanUpDatabase();
  });

  describe('Hitting /original.txt endpoint', () => {

    it('When jwtToken is not provided, it should return status 401', async () => {
      const response = await chai.request(app).get("/assets/original.txt")
      expect(response).to.have.status(401);
    });

    it('When jwtToken is provided, it should return status 200', async () => {
      const agent = chai.request.agent(app);
      await agent.post("/login").send({ username, password });
      const response = await agent.get("/assets/original.txt")
      expect(response).to.have.status(200);
    });

  });

  describe('Hitting /sorted.txt endpoint', () => {

    it('When jwtToken is not provided, it should return status 401', async () => {
      const response = await chai.request(app).get("/assets/sorted.txt")
      expect(response).to.have.status(401);
    });

    it('When jwtToken is provided and any sort operation is done, it should return status 200', async () => {
      const agent = chai.request.agent(app);
      await agent.post("/login").send({ username, password });
      await agent.get("/des")
      const response = await agent.get("/assets/sorted.txt")
      expect(response).to.have.status(200);
    });

  });

  describe('Hitting /log.txt endpoint', () => {

    it('when jwtToken is not provided, it should return status 401', async () => {
      const response = await chai.request(app).get("/assets/log.txt")
      expect(response).to.have.status(401);
    });

    it('When jwtToken is provided, it should return status 200', async () => {
      const agent = chai.request.agent(app);
      await agent.post("/login").send({ username, password });
      const response = await agent.get("/assets/log.txt")
      expect(response).to.have.status(200);
    });

  });

});