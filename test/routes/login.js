require("dotenv").config({ path: "./.env.test" });
const User = require("../../models/User");
const chai = require('chai');
const { expect } = chai
const { cleanUpDatabase } = require("../../utils/database");
const app = require("../../app");
const http = require('chai-http');

chai.use(http);

describe('Login route /login', () => {
  const username = "John Doe";
  const password = "123456";

  before(async () => {
    const user = new User({ username, password });
    await user.save();
  });

  after(async () => {
    await cleanUpDatabase();
  });

  describe('On Post request', () => {

    it('When right credentials are provided, it should return status 200 and jwtToken in cookies ', async () => {
      const response = await chai.request(app).post("/login").send({ username, password });
      expect(response).to.have.cookie("jwtToken");
      expect(response).to.have.status(200);
    });

    it('When wrong credentials are provided, it should return status 401 and do not have jwtToken in cookies', async () => {
      const response = await chai.request(app).post("/login").send({ username, password: "654321" });
      expect(response).to.not.have.cookie("jwtToken");
      expect(response).to.have.status(401);
    });

  });

});