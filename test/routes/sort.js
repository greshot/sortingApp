require("dotenv").config({ path: "./.env.test" });
const User = require("../../models/User");
const chai = require('chai');
const { expect } = chai
const { cleanUpDatabase } = require("../../utils/database");
const app = require("../../app");
const http = require('chai-http');

chai.use(http);



describe('Sorting route /', () => {
  const username = "John Doe";
  const password = "123456";

  before(async () => {
    const user = new User({ username, password });
    await user.save();
  });

  after(async () => {
    await cleanUpDatabase();
  });

  describe('Hitting /asc endpoint', () => {

    it('When jwtToken is not provided, it should return status 401', async () => {
      const response = await chai.request(app).get("/asc")
      expect(response).to.have.status(401);
    });

    it('When jwtToken is provided, it should return status 200 and data in body', async () => {
      const agent = chai.request.agent(app);
      await agent.post("/login").send({ username, password });
      const response = await agent.get("/asc")
      expect(response).to.have.status(200);
      expect(response.body).to.have.property("data");
      expect(response.body.data).to.be.an("Array");
    });

  });

  describe('Hitting /des endpoint', () => {

    it('When jwtToken is not provided, it should return status 401', async () => {
      const response = await chai.request(app).get("/des")
      expect(response).to.have.status(401);
    });

    it('When jwtToken is provided, it should return status 200 and data in body', async () => {
      const agent = chai.request.agent(app);
      await agent.post("/login").send({ username, password });
      const response = await agent.get("/des")
      expect(response).to.have.status(200);
      expect(response.body).to.have.property("data");
      expect(response.body.data).to.be.an("Array");
    });

  });

  describe('Hitting /mix endpoint', () => {

    it('When jwtToken is not provided, it should return status 401', async () => {
      const response = await chai.request(app).get("/mix")
      expect(response).to.have.status(401);
    });

    it('When jwtToken is provided, it should return status 200 and data in body', async () => {
      const agent = chai.request.agent(app);
      await agent.post("/login").send({ username, password });
      const response = await agent.get("/mix")
      expect(response).to.have.status(200);
      expect(response.body).to.have.property("data");
      expect(response.body.data).to.be.an("Array");
    });

  });

});