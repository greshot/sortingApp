require("dotenv").config({ path: "./.env.test" });
const chai = require('chai');
const { expect } = chai
const { cleanUpDatabase } = require("../../utils/database");
const app = require("../../app");
const http = require('chai-http');

chai.use(http);

describe('Users route /users', () => {

  after(async () => {
    await cleanUpDatabase();
  });

  describe('On Post request', () => {

    it('When information is not complete it should return status 500, have error in body and do not have jwtToken in cookies', async () => {
      const response = await chai.request(app).post("/users").send({ username: "John Doe" });
      expect(response).to.have.status(500);
      expect(response.body).to.have.property("error");
      expect(response).to.not.have.cookie("jwtToken");
    });

    it('When information is not complete it should return status 201, do not have error in body and have jwtToken in cookies', async () => {
      const response = await chai.request(app).post("/users").send({ username: "John Doe", password: "123456" });
      expect(response).to.have.status(201);
      expect(response.body).to.not.have.property("error");
      expect(response).to.have.cookie("jwtToken");
    });


  });

});