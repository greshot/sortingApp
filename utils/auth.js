const jwt = require("jsonwebtoken");

exports.generateToken = id => {
  return new Promise((resolve, reject) => {
    jwt.sign(
      {
        id: id
      },
      process.env.JWT_KEY,
      {
        expiresIn: "1h"
      },
      (error, token) => {
        if (error) {
          console.log("error while generating token ", error);
          reject();
        } else {
          resolve(token);
        }
      }
    );
  });
};
