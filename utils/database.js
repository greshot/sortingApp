const mongoose = require("mongoose");

exports.connectToDatabase = () => {
  return mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
};

exports.cleanUpDatabase = () => {
  return mongoose.connection.db.dropCollection("users");
};

exports.closeConnection = () => {
  return mongoose.connection.close();
};
