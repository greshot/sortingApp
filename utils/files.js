const fs = require("fs");
const path = require("path");
const readLine = require("readline");
const eol = require("os").EOL;

const originalFile = path.join("./", "assets", "original.txt");
const sortedFile = path.join("./", "assets", "sorted.txt");

const parseOriginalFile = () =>
  new Promise((resolve, reject) => {
    const data = [];
    const lineReader = readLine.createInterface({
      input: fs.createReadStream(originalFile, {
        encoding: "utf8",
        autoClose: true
      })
    });
    lineReader
      .on("line", line => {
        const parsedLine = line
          .toString()
          .replace(/\[|\]|;/g, "")
          .split(",");
        data.push(parsedLine);
      })
      .on("close", () => {
        if (!data.length) reject();
        resolve(data);
      });
  });

const saveSortedData = data =>
  new Promise((resolve, reject) => {
    let parsedData = "";
    data.forEach(element => {
      const parsedElement = `[${element.join()}];`;
      parsedData += `${parsedElement}${eol}`;
    });

    fs.writeFile(sortedFile, parsedData, error => {
      if (error) {
        reject();
      }
      resolve();
    });
  });

module.exports = {
  parseOriginalFile,
  saveSortedData
};
