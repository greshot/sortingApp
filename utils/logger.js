const { createLogger, format, transports } = require("winston");
const path = require("path");

module.exports = createLogger({
  format: format.simple(),
  transports: [
    new transports.Console(),
    new transports.File({ filename: path.join("./", "assets", "log.txt") })
  ]
});
